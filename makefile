all :
	gcc single_shot1.cpp -o single1
	gcc single_shot2.cpp -o single2
	gcc multiple.cpp -o multiple

single1 :
	gcc single_shot1.cpp -o single1

single2 :
	gcc single_shot2.cpp -o single2

multiple :
	gcc multiple.cpp -o multiple

pres1 :
	gcc single_shot1.cpp -o single1
	./single1

pres2 :
	gcc single_shot2.cpp -o single2
	./single2 -v

pres3 :
	gcc multiple.cpp -o multiple
	./multiple

.PHONY : pres1
.PHONY : pres2
.PHONY : pres3



